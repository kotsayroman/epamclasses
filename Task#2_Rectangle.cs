using System;

namespace Epam.Tasks.ClassRectangle
{
	struct Point
	{		
		public Point(double x, double y)
		{
			_x = x;
			_y = y;
		}
		
		public double X 
		{
			get
			{
				return _x;
			}
			set
			{
				_x = value;
			}
		}
		public double Y 
		{
			get
			{
				return _y;
			}
			set
			{
				_y = value;
			}
		}
		
		private double _x;
		private double _y;
	}
	
	class Rectangle
	{
		public Rectangle()
		{
			_location = new Point();
			_width = 0;
			_height = 0;
		}
		public Rectangle(double width, double height, Point location)
		{
			_width = (width > 0) ? width : 0;
			_height = (height > 0) ? height : 0;
			_location = location;
		}
		public Rectangle(double width, double height, double x, double y)
		{
			_width = (width > 0) ? width : 0;
			_height = (height > 0) ? height : 0;
			_location = new Point(x, y);
		}
		
		public double Width
		{
			get
			{
				return _width;
			}
			set
			{
				_width = (value > 0) ? value : 0;
			}
		}
		public double Height
		{
			get
			{
				return _height;
			}
			set
			{
				_height = (value > 0) ? value : 0;
			}
		}
		public double X
		{
			get
			{
				return _location.X;
			}
		}
		public double Y
		{
			get
			{
				return _location.Y;
			}
		}
		
		public override bool Equals(object obj)
		{
			Rectangle other = obj as Rectangle;
			
			if (other == null)
			{
				return false;
			}
			else
			{
				return (Width == other.Width) &&
				(Height == other.Height) &&
				(X == other.X) &&
				(Y == other.Y);
			}
		}				
		public override int GetHashCode()
		{
			int hash = 15;
			
			hash = hash * 23 + Width.GetHashCode();
			hash = hash * 23 + Height.GetHashCode();
			hash = hash * 23 + X.GetHashCode();
			
			return hash;
		}
		public override string ToString()
		{
			return String.Concat("(width = ", Width, "; height = ", Height, "; location: {", X, "; ", Y, "})");
		}
		
		public void Move(double deltaX, double deltaY)
		{
			_location.X += deltaX;
			_location.Y += deltaY;
		}
		public void MoveTo(double x, double y)
		{
			_location.X = x;
			_location.Y = y;
		}
		public void MoveTo(Point destination)
		{
			_location.X = destination.X;
			_location.Y = destination.Y;
		}
		
		public static Rectangle LeastContainer(Rectangle first, Rectangle second)
		{
			Rectangle container = new Rectangle();
			
			double containerLeftX = (first.LeftSideX < second.LeftSideX) ? first.LeftSideX : second.LeftSideX;
			double containerRightX = (first.RightSideX > second.RightSideX) ? first.RightSideX : second.RightSideX;
			
			container.Width = containerRightX - containerLeftX;
			container._location.X = (containerLeftX + containerRightX) / 2;
			
			double containerUpY = (first.UpSideY > second.UpSideY) ? first.UpSideY : second.UpSideY;
			double containerDownY = (first.DownSideY < second.DownSideY) ? first.DownSideY : second.DownSideY;
		    
			container.Height = containerUpY - containerDownY;
			container._location.Y = (containerUpY + containerDownY) / 2;
			
			return container;
		}
		public static Rectangle ResultOfCrossing(Rectangle first, Rectangle second)
		{
			if (first.Contain(second))
			{
				return second;
			}
			else if (second.Contain(first))
			{
				return first;
			}
			else
			{
				Rectangle leftRectangle = (first.X <= second.X) ? first : second;
				Rectangle rightRectangle = (first.X > second.X) ? first : second;
				Rectangle upperRectangle = (first.Y >= second.Y) ? first : second;
				Rectangle lowerRectangle = (first.Y < second.Y) ? first : second;
			
				double crossLeftX = rightRectangle.LeftSideX;
				double crossRightX = leftRectangle.RightSideX;
				double crossUpY = lowerRectangle.UpSideY;
				double crossDownY = upperRectangle.DownSideY;
				if ((crossLeftX > crossRightX) || (crossDownY > crossUpY))
				{
					return new Rectangle();
				}
				else
				{
					return new Rectangle(crossRightX - crossLeftX, crossUpY - crossDownY, (crossLeftX + crossRightX) / 2, (crossDownY + crossUpY) / 2);
				}								
			}			
		}
		
		public static bool operator ==(Rectangle first, Rectangle second)
		{
			return first.Equals(second);
		}
		public static bool operator !=(Rectangle first, Rectangle second)
		{
			return !first.Equals(second);
		}	
			
		private double _width;
		private double _height;
		private Point _location;
		
		private double LeftSideX
		{
			get
			{
				return (X - Width / 2);
			}
		}
		private double RightSideX
		{
			get
			{
				return (X + Width / 2);
			}
		}
		private double UpSideY
		{
			get
			{
				return (Y + Height / 2);
			}
		}
		private double DownSideY
		{
			get
			{
				return (Y - Height / 2);
			}
		}
		
		private bool Contain(Rectangle other)
		{
			if ((LeftSideX <= other.LeftSideX) && (RightSideX >= other.RightSideX) && (UpSideY >= other.UpSideY) && (DownSideY <= other.DownSideY))
			{
				return true;
			}
			else 
			{
				return false;
			}
		}
	}
	
	class RepresentingRectangle
	{
		static void Main()
		{
			// Creating new rectangles
			var rect1 = new Rectangle(2, 2, new Point(0, 0));
			var rect2 = new Rectangle(2, 2, new Point(1, 1));
			var rect3 = new Rectangle(1, 1, 7.5, 2.5);
			Console.WriteLine(rect1.ToString());
			Console.WriteLine(rect2.ToString());
			Console.WriteLine(rect3.ToString());
			Console.WriteLine();
			
			// Moving rectangle
			rect1.Move(3, 2.5);
			Console.WriteLine("rect1 moved: {0}", rect1.ToString());
			
			rect1.MoveTo(3434, 3);
			Console.WriteLine("rect1 moved: {0}", rect1.ToString());
			
			rect1.MoveTo(new Point(5.5, 3));
			Console.WriteLine("rect1 moved: {0}", rect1.ToString());
			Console.WriteLine();
			
			// Changing rectangle`s size
			rect1.Width = 5;
			rect1.Height = 3;
			Console.WriteLine("rect1 changed size: {0}", rect1);
			Console.WriteLine();
			rect1 = new Rectangle(2, 2, new Point(0, 0));
			
			// Creating new rectangle, which contains two others
			Rectangle container = Rectangle.LeastContainer(rect1, rect2);
			Console.WriteLine("The least rectangle, which contains rect1 and rect2 is {0}", container);
			
			// Creating new rectangle formed at the intersection of to others
			Rectangle intersection = Rectangle.ResultOfCrossing(rect1, rect2);
			Console.WriteLine("The rectangle formed at the intersection of rect1 and rect2 is: {0}", intersection);	
		}
	}
}